const items = require('../lib/items');
const db = require('..');

db.connect(() => {
  items.getTags((err, tags) => {
    db.close();
    if (err)
    { console.error(err); }
    console.dir(tags);
  });
});
