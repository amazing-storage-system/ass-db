const fs = require('fs');
const db = require('..');
const async = require('async');

var jsonData = JSON.parse(fs.readFileSync('./data/containers.json'));

var inserted=0;

function createContainer(item,cb){

  if(!item)
    return cb();
  if(!item.iata)
    return cb(new Error("iata not provided"));
  if(!item.title)
    return cb(new Error("title not provided"));
  if(!item.type)
    return cb(new Error("type not provided"));

  db.IataModel.findOne({"iata": item.iata}, function(err, iata){

    if(err)return cb(err);
    if(!iata) return cb(new Error("iata not found"));

    db.ContainerTypeModel.findOne({"title": item.type }, function(err, containertype){

      if(err)return cb(err);
      if(!containertype) return cb(new Error("containertype not found"));

      db.ContainerModel.create({
        title: item.title,
        code: {
          id: "box://"+iata.iata,
          iata: iata._id,
        },
        type: containertype._id
      }, function(err, created){
        if(err){
          console.error(err);
          console.dir(item);
          return cb(err);
        } else {
          inserted++;
          return cb();
        }
      });
    });
  });
}

db.connect(function(err){

  if(err){
    console.error(err);
    process.exit(-1);
  }

  db.ContainerModel.deleteMany({},function(err,removed){

    if(err){
      console.error(err);
      return;
    }

    console.log(removed.n +' documents removed');

    async.each(jsonData, createContainer, function(err){
      if(err){
        console.error(err);
      }
      console.log(inserted+" documents inserted");
      process.exit(0);
    });
  });
});











