const fs = require('fs');
const db = require('../');
const async = require('async');

var insertedFirstNames=0;
var insertedLastNames=0;

function createFirstName(item, cb){

    db.FirstNameModel.create({firstname: item}, function(err,created){
        if(err){
            if(err.code==11000){
                return cb();
            }
            console.error(err);
            console.dir(item);
            return cb(err);
        } else {
          insertedFirstNames++;
          process.stdout.write('\r' + insertedFirstNames);
          return cb();
        }
    });
}

function createLastName(item, cb){

  db.LastNameModel.create({lastname: item}, function(err,created){

      if(err){
        if(err.code==11000){
            return cb();
        }
        console.error(err);
        console.dir(item);
        return cb(err);
      } else {
        insertedLastNames++;
        process.stdout.write('\r' + insertedLastNames);
        return cb();
      }
  });
}

function createLastNames(callback){

  console.log('Parsing lastnames JSON');
  var jsonLastNames = JSON.parse(fs.readFileSync('./data/lastnames.json'));
  console.log('Done ! %d last names to insert', jsonLastNames.length);

  db.LastNameModel.deleteMany({},function(err,removed){

    if(err){
        console.error(err);
        return callback(err);
    }

    console.log(removed.n +' lastnames removed');
    async.eachSeries(jsonLastNames, createLastName, function(err){
        if(err){
            console.error(err);
            return callback(err);
        }
        process.stdout.write('\n');
        console.log(insertedLastNames+" lastnames inserted");
        return callback(false, insertedLastNames);
    });
  });
}

function createFirstNames(callback){

  console.log('Parsing firstnames JSON');
  var jsonFirstNames = JSON.parse(fs.readFileSync('./data/firstnames.json'));
  console.log('Done ! %d first names to insert', jsonFirstNames.length);

  db.FirstNameModel.deleteMany({},function(err,removed){

    if(err){
        console.error(err);
        return callback(err);
    }

    console.log(removed.n +' firstnames removed');
    async.eachSeries(jsonFirstNames, createFirstName, function(err){
        if(err){
            console.error(err);
            return callback(err);
        }
        process.stdout.write('\n');
        console.log(insertedFirstNames+" firstnames inserted");
        return callback(false, insertedFirstNames);
    });
  });
}

db.connect(function(err){
    if(err){
        console.error(err);
        process.exit(-1);
    }
    async.applyEachSeries([createFirstNames,createLastNames])(function(err){
      if(err) {
        console.error(err);
        process.exit(-1);
      }
      process.exit(0);
    });
});











