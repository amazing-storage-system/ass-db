const fs = require('fs');
const db = require('../');
const async = require('async');

var jsonData = JSON.parse(fs.readFileSync('./data/airports.json'));

var inserted=0;

function createIata(item,cb){

    if(!item.iata)
        return cb();
    if(!item.city)
        return cb();

    db.IataModel.create(item, function(err,created){
        if(err){
            console.error(err);
            console.dir(item);
            //process.exit(-1);
            return cb(err);
        } else {
            inserted++;
            return cb();
        }
    });
}

db.connect(function(err){

    if(err){
        console.error(err);
        process.exit(-1);
    }

    db.IataModel.remove({},function(err,removed){

        if(err){
            console.error(err);
            return;
        }

        console.log(removed.n +' documents removed');

        async.each(jsonData, createIata, function(err){
            if(err){
                console.error(err);
            }
            console.log(inserted+" documents inserted");
            process.exit(0);
        });
    });
});











