const fs = require('fs');
const db = require('..');
const async = require('async');

var jsonData = JSON.parse(fs.readFileSync('./data/container-types.json'));

var inserted=0;

function createContainerType(item,cb){

    if(!item)
        return cb();

    db.ContainerTypeModel.create({title: item}, function(err, created){
        if(err){
            console.error(err);
            console.dir(item);
            //process.exit(-1);
            return cb(err);
        } else {
            inserted++;
            return cb();
        }
    });
}

db.connect(function(err){

    if(err){
        console.error(err);
        process.exit(-1);
    }

    db.ContainerTypeModel.deleteMany({},function(err,removed){

        if(err){
            console.error(err);
            return;
        }

        console.log(removed.n +' documents removed');

        async.each(jsonData, createContainerType, function(err){
            if(err){
                console.error(err);
            }
            console.log(inserted+" documents inserted");
            process.exit(0);
        });
    });
});











