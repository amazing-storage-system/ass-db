var iatas = require('../lib/iata');
var db = require('../');

db.connect(function(err){
    iatas.getRandomNotUsed(1, function(err,items){
        db.close();
        if(err)
            console.error(err);
        console.log("got " + items.length + " random iata:" );
        var codes=items.map(function(item){
            return item.iata;
        });
        console.dir(codes);
    })
});
