const db = require('../');

var Items={};

Items.getItems = function(cb){
  db.ItemModel
    .find({})
    .populate('type')
    .populate('located.location')
    .populate('shared')
    .exec(function(err, containers){
      if(err)
        return cb(err);
      return cb(false,containers);
    });
};

Items.getById = function(itemid, cb){
  db.ItemModel
    .find({"id": itemid})
    .populate('type')
    .populate('located.location')
    .populate('shared')
    .exec(function(err, item){
      if(err)
        return cb(err);
      return cb(false,item);
    });
};

Items.getTags = function(cb){
  db.ItemModel.find({}, "tags", function(err, itemtags){
    if(err)
      return cb(err);
    return cb(false,itemtags.map(function(item){
      return item.tags.map(function(tag){
        return tag;
      });
    }));
  });
};

Items.create = function(requestedItem, cb){
  db.ItemModel.create({
    code: {
      id:     requestedItem.id,
      name:   requestedItem.name,
      uan:    requestedItem.uan
    },
    title: requestedItem.title,
    type: requestedItem.itemtypeid,
  },
  function(err, created){
    if(err) return cb(err);
    if(!created) return cb(new Error("not created"));

    return cb(false, created);
  });
};

Items.update = function(requestedItem, cb) {
  return cb(new Error('Not implemented'));
}
//Decrease item count or item weight depending on unit
Items.decrease= function(itemid, amount, cb) {
  db.ItemModel.find({id: itemid},function(err, item) {
    if(err)
      return cb(err);
    //TODO: decrease
    return cb(new Error('Not implemented'));
  });
};

module.exports=Items;