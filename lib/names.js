const db = require('../');
const async = require('async');

var Names={};

function getRandomArbitrary(min, max) {
    return Math.ceil(Math.random() * (max - min) + min);
}

function getRandomFirstName(cb){
    db.FirstNameModel.countDocuments({}, function(err, count){
        if(err)
            return cb(err);
        var skipRecords = getRandomArbitrary(1, count-1);
        db.FirstNameModel
            .find({})
            .skip(skipRecords)
            .limit(1)
            .exec(function(err, names){
                if(err)
                    return cb(err);
                return cb(false, names[0].firstname);
            });
    });
}

function getRandomLastName(cb){
    db.LastNameModel.countDocuments({}, function(err, count){
        if(err)
            return cb(err);
        var skipRecords = getRandomArbitrary(1, count-1);
        db.LastNameModel
            .find({})
            .skip(skipRecords)
            .limit(1)
            .exec(function(err, names){
                if(err)
                    return cb(err);
                return cb(false, names[0].lastname);
            });
    });
}

Names.getFirstNamesStartingWith = function(start, cb){
    var regexp = new RegExp("^"+ start, "i");
    db.FirstNameModel.find({firstname: regexp}, function(err,docs){
        if(err) return cb(err);
        if(docs){
            var names = docs.map(function(doc){
                return doc.firstname;
            });
            return cb(false, names);
        }
        return cb(false, []);
    });
};

Names.getLastNamesStartingWith = function(start, cb){
    var regexp = new RegExp("^"+ start, "i");
    db.LastNameModel.find({lastname: regexp}, function(err,docs){
        if(err) return cb(err);
        if(docs){
            var names = docs.map(function(doc){
                return doc.lastname;
            });
            return cb(false, names);
        }
        return cb(false, []);
    });
};

Names.getRandomName = function(cb){
    async.parallel({
        firstname:  getRandomFirstName,
        lastname: getRandomLastName
    }, function(err, results) {
        if(err) return cb(err);
        return cb(false,results);
    });
};

module.exports=Names;