const db = require('../');

var Iata={};

function getRandomArbitrary(min, max) {
    return Math.ceil(Math.random() * (max - min) + min);
}

Iata.getRandomNotUsed = function(limit, cb){

    //get all containers codes
    db.ContainerModel.find({},'code',function(err, docs){

        if(err)
            return cb(err);

        var usedIatas= docs.map(function(container){
            return container.code.iata;
        });

        //Find iatas not in codes
        db.IataModel.countDocuments({iata: { $nin: usedIatas}}, function(err, count){
            if(err)
                return cb(err);

            var skipRecords = getRandomArbitrary(1, count-limit);
            db.IataModel
                .find({iata: { $nin: usedIatas}})
                .skip(skipRecords)
                .limit(limit)
                .exec(function(err, iatas){
                    if(err)
                        return cb(err);
                    return cb(false, iatas);
                });
        });
    });
}

module.exports=Iata;