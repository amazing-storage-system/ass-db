const db = require('../');

var Containers={};

Containers.create = function(requestedContainer, cb) {
  db.IataModel.findOne({"iata": requestedContainer.iata}, function(err, iata){
    if(err) return cb(err);
    if(!iata) cb(new Error("iata not found"));

    db.ContainerTypeModel.findOne({"title": requestedContainer.containertype }, function(err, containertype){
      if(err) return cb(err);
      if(!containertype) return next(new Error("containertype not found"));

      db.ContainerModel.create({
        code: {
          id:     requestedContainer.id,
          iata:   iata._id
        },
        title: requestedContainer.title,
        type: containertype._id,
      },
      function(err, created){
        if(err) return cb(err);
        if(!created) return cb(new Error("not created"));

        return cb(false, created);
      });
    });
  });
}

Containers.getContainers = function(cb){
  db.ContainerModel
    .find({})
    .populate('code.iata')
    .populate('type')
    .populate('location')
    .exec(function(err, containers){
      if(err)
        return cb(err);
      return cb(false,containers);
    });
};

Containers.getContainerTypes= function(cb){
  db.ContainerTypeModel.find({}, function(err, containertypes){
    if(err)
      return cb(err);
    return cb(false,containertypes);
  });
};

Containers.getByIata = function(iata, cb) {
  return cb(new Error('Not implemented'));
};

Containers.getById = function(id, cb) {
  return cb(new Error('Not implemented'));
};

Containers.update = function(requestedContainer, cb) {
  return cb(new Error('Not implemented'))
};

Containers.delete = function(id, cb) {
  return cb(new Error('Not implemented'))
};

module.exports=Containers;