
const mongoose = require('mongoose');

module.exports = {
    ContainerTypeModel: require('./models/container-type'),
    ContainerModel: require('./models/container'),
    IataModel: require('./models/iata'),
    ItemModel: require('./models/item'),
    FirstNameModel: require('./models/firstname'),
    LastNameModel: require('./models/lastname'),
    connect: function(cb){
        mongoose.connect('mongodb://192.168.77.253:32768/ass-db',{ useNewUrlParser: true,  useCreateIndex: true, useUnifiedTopology: true}, cb);
    },
    close: function(cb){
        mongoose.connection.close(cb);
    }
};

