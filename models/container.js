var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ContainerSchema = new Schema({
    dateCreated:    { type: Date, default: Date.now },
    dateDeleted:    { type: Date, default: null },
    code:           {
        id: {type: String, required: true, index: { unique: true}},
        iata: { type: mongoose.Schema.Types.ObjectId, ref: 'Iata'},
    },
    title:          { type: String, required: true},
    type:           { type: mongoose.Schema.Types.ObjectId, ref: 'ContainerType', required: true},
    location:       { type: mongoose.Schema.Types.ObjectId, ref: 'Container', default: null},
    movable:        { type: Boolean, required: false , default: false}
});

var ContainerModel = mongoose.model('Container', ContainerSchema);

module.exports = ContainerModel;