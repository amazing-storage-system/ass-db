var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ContainerTypeSchema = new Schema({
    dateCreated: { type: Date, default: Date.now },
    dateDeleted: { type: Date, default: null },
    title:   {type: String, required: true},
});

var ContainerTypeModel = mongoose.model('ContainerType', ContainerTypeSchema);

module.exports = ContainerTypeModel;