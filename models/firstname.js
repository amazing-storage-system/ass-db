var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FirstNameSchema = new Schema({
  //country:      {type: String, required: true},
  firstname:    {type: String, required: true, index: { unique: true, dropDups: true }},
});

var FirstNameModel = mongoose.model('FirstName', FirstNameSchema);

module.exports = FirstNameModel;