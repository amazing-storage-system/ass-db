var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LastNameSchema = new Schema({
  //country:    {type: String, required: true},
  lastname:   {type: String, required: true, index: { unique: true, dropDups: true }},
});

var LastNameModel = mongoose.model('LastName', LastNameSchema);

module.exports = LastNameModel;