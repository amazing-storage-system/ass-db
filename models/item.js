var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ItemSchema = new Schema({


    //Required

    dateCreated:    { type: Date, default: Date.now },
    dateDeleted:    { type: Date, default: null },

    code: {
      id:   { type: String, required: true, index: { unique: true}},
      uan:  { type: String },
      name: { type: String }
    },

    title:  { type: String, required: true},
    type:   { type: mongoose.Schema.Types.ObjectId, ref: 'ItemType', required: true},



    //Optionnal

    //item tags
    tags: [
      { type: String }
    ],

    //item details
    details : {
      description:      { type: String },
      serial:           { type: String },
      color:            { type: String },
      weight: {
        value:          { type: String },
        unit:           { type: String },
      },
      volume:           { type: String },
      dimensions:       { type: String },
      fragile:          { type: Boolean },
      recommendations:  { type: String },
      pictures: [
        { type: Object }
      ],
    },

    //perishable data
    perishable: {
      dlc: {
        date:   { type: Date },
        alert:  { type: Boolean },
      },
      dluo: {
        date:   { type: Date },
        alert:  { type: Boolean },
      },
    },

    //item quantity
    quantify: {
      qty:      { type: Number },
      unit:     { type: String },
      min: {
        value:  { type: Number },
        alert:  { type: Boolean },
      },
      max: {
        value:  { type: Number },
        alert:  { type: Boolean },
      }
    },

    //tank capacity
    receptacle: {
      unit:     { type: String },
      capacity: { type: Number },
      min: {
        value:  { type: Number },
        alert:  { type: Boolean },
      },
      max: {
        value:  { type: Number },
        alert:  { type: Boolean },
      }
    },

    //location
    located: {
      location:         { type: mongoose.Schema.Types.ObjectId, ref: 'Container'},
      recommendations:  { type: String },
    },

    shared:  [{ type: Schema.ObjectId, ref: 'ItemShare' }],

});

var ItemModel = mongoose.model('Item', ItemSchema);

module.exports = ItemModel;