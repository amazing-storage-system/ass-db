var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ItemTypeSchema = new Schema({
    dateCreated: { type: Date, default: Date.now },
    dateDeleted: { type: Date, default: null },
    title:   {type: String, required: true},
});

var ItemTypeModel = mongoose.model('ItemType', ItemTypeSchema);

module.exports = ItemTypeModel;