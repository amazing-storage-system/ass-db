var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var IataSchema = new Schema({
    name:       {type: String, required: true},
    city:       {type: String, required: true},
    country:    {type: String, required: true},
    iata:       {type: String, required: true, index: { unique: true, dropDups: true }},
    icao:       {type: String, required: true},
    latitude:   {type: Number, required: true},
    longitude:  {type: Number, required: true},
    altitude:   {type: Number, required: true},
    timezone:   {type: Number, required: false},
    dst:        {type: String, required: false},
});

var IataModel = mongoose.model('Iata', IataSchema);

module.exports = IataModel;